const TelegramApi = require('node-telegram-bot-api');
const {gameOptions, againOptions} = require('./options');
const token = '1999512110:AAFHahwsu-wrJO97o6mpiggJVg0zPmJN2L8';

const bot = new TelegramApi(token, {polling: true});

const chats = {}



const startGame = async (chatId) => {
    await bot.sendMessage(chatId, `сейчас я загадаю число от 0 до 9, а ты должен его угадать`);
    const randomNumber = Math.floor(Math.random() * 10)
    chats[chatId] = randomNumber
    await bot.sendMessage(chatId, 'Отгадывай', gameOptions)
}

const start =  () => {
     bot.setMyCommands([
        {command: '/start', description: 'Начальное приветсвие'},
        {command: '/info', description: 'получить информацию'},
        {command: '/game', description: 'Игра угадай цифру'},
    ])

    bot.on('message',  async msg => {
        const text = msg.text;
        const chatId = msg.chat.id;
        if (text === '/start') {
            await bot.sendSticker(chatId, 'https://tlgrm.ru/_/stickers/b08/942/b08942f6-9ad5-38ca-99b6-8e79908886fd/3.webp')
            return  bot.sendMessage(chatId, `Добро пожаловать ${msg.from.username}, это канал Erzhan'a`);
        }
        if (text === '/info') {
            return  bot.sendMessage(chatId, `Тебя зовут ${msg.from.first_name}`);
        }

        if (text === '/game'){
            return startGame(chatId)
        }
            return bot.sendMessage(chatId, `Я тебя не понимаю ${msg.from.first_name}`);
    });

     bot.on('callback_query',  async msg => {
         const data = msg.data;
         const chatId = msg.message.chat.id;
         if (data === '/again'){
            return startGame(chatId)
         }

         if(data === chats[chatId]) {
             return  bot.sendMessage(chatId, `Поздравляю ты отгадал цифру ${chats[chatId]}`, againOptions);
         } else {
             return bot.sendMessage(chatId, `К сожалению ты не отгадал , бот загадал цифру ${chats[chatId]}`, againOptions);
         }
     });
};

start();